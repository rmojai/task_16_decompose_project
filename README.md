# Task_16_Decompose_project

Analyze and decompose DjangoWeatherRemider. 
You should make a research and describe project structure. 
Create UML class diagram for application and describe each module etc.

Storyline
Write the application like a service of weather notification in the subscribed city via email, 
webhook.  
DjangoWheatherRemider application is provided API for getting information about the weather. 
User (or third-party service) can register and get auth token like a JWT (JSON web token)
The user can subscribe/unsubscribe to one or a few cities with parameters 
like a period of notification(1, 3, 6, 12 hours), etc. 
The user can edit parameters of subscribing or delete. 
The user can get a list of subscribing. 
Use third-party services for getting actual weather data.  